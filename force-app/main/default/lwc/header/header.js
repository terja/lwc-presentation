import { LightningElement, track } from 'lwc';
import CUSTOM_LOGO from '@salesforce/resourceUrl/customAstro';
import TRAILHEAD_LOGO from '@salesforce/resourceUrl/trailheadLogo';
import JS_LOGO from '@salesforce/resourceUrl/jsLogo';
import ALL_MIGHT from '@salesforce/resourceUrl/allmight';
import LWC from '@salesforce/resourceUrl/lwc';
import SIMPSONS_MEME from '@salesforce/resourceUrl/sMeme';
import VS_EXAMPLE from '@salesforce/resourceUrl/vsExample';
import WHAT from '@salesforce/label/c.whatAre';
import VS from '@salesforce/label/c.vs';
import SFDX from '@salesforce/label/c.sfdx';
import DX_CLI from '@salesforce/resourceUrl/sfdx';
import SIMPSONS_MEME2 from '@salesforce/resourceUrl/sMeme2';
import RESOURCES from '@salesforce/label/c.resources';
import IGGY from '@salesforce/resourceUrl/iggy';

export default class Header extends LightningElement {
    customLogo = CUSTOM_LOGO;
    trailheadLogo = TRAILHEAD_LOGO;
    jsLogo = JS_LOGO;
    allmight = ALL_MIGHT;
    image = null;
    lwc = LWC;
    headingText = 'Lightning web components by Jaime Terrats';
    options = [
            { id: 'home', value: 'Inicio' },
            { id: 'what', value: 'Qué son?' },
            { id: 'vs', value: 'Lightning Component vs LWC' },
            { id: 'sfdx', value: 'SFDX' },
            { id: 'questions', value: 'Preguntas' },
            { id: 'resources', value: 'Recursos'},
            { id: 'end', value: 'Fin' }
    ];

    @track startPresentation = true;
    @track script = '';
    @track hasImage = false;
    @track endImage = false;

    handleNavSelection(event) {
        const selectedOption = event.currentTarget.value;
        if(selectedOption === 'home') {
            this.startPresentation = true;
            this.assignVisibility('', null, false, false);
        } else if(selectedOption === 'what') {
            this.startPresentation = false;
            this.assignVisibility(WHAT, SIMPSONS_MEME, true, false);
        } else if(selectedOption === 'vs') {
            this.startPresentation = false;
            this.assignVisibility(VS, VS_EXAMPLE, true, false);
        } else if(selectedOption === 'sfdx') {
            this.startPresentation = false;
            this.assignVisibility(SFDX, DX_CLI, true, false);
        } else if(selectedOption === 'questions') {
            this.startPresentation = false;
            this.assignVisibility('', SIMPSONS_MEME2, true, false);
        } else if(selectedOption === 'resources') {
            this.startPresentation = false;
            this.assignVisibility(RESOURCES, null, false, false);
        } else if(selectedOption === 'end') {
            this.startPresentation = false;
            this.assignVisibility('', IGGY, false, true);
        }
    }

    assignVisibility(script, image, hasImage, endImage) {
        this.script = script;
        this.image = image;
        this.hasImage = hasImage;
        this.endImage = endImage;
    }
}