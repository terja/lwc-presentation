import { LightningElement, api } from 'lwc';

export default class Content extends LightningElement {
    @api script;
    @api image;
    @api hasImage;
    @api endImage;
}