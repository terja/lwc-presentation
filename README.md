# Salesforce App

## Part 1: Project Reason

This guide helps Salesforce developers who are new to Visual Studio Code go from zero to a deployed app using Salesforce Extensions for VS Code and Salesforce CLI.

Besides the auto generated text, this was think to give a **lightning - talk** about LWC framework.

## Part 2: How to use it

requirements: Node.js, SFDX, VSCode

install modules: npm i

push project to your developer org: sfdx force:source:deploy -p force-app/ -u <your@username.com> or org-alias

## Part 3: Project License

This is an MIT license project, please if you fork/clone this project do the propper referral to this repo, thank you.

## Part 4: About the Author

Im Jaime Terrats, currently developing & designing software architecture @ Softtek, world wide, **Cat Lover** and someties **Musician**. Main interests
**_Salesforce_**, **_MERN_**, **_MEAN_**, **_MEAUN_**, **_OpenWRT_**, **_AWS_**, **_RN_**, **_Scrum_**, **_DotNet Core_**,
**_Arch Linux_**, **_Embedded_**. If you have any doubts please ping at my email: jaime.terrats@softtek.com
I'll try to get back to you asap.

Want to know more about this topic, check my guide  [GIT - SFDX](https://gitlab.com/terja/sfdx-git)

Happy coding and may the **Force.com** be with you!
